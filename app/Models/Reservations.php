<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Reservations extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    //protected $connection = 'mongodb';
    //protected $collection = 'reservations';
    public $table = 'reservations';
    protected $fillable = [
        'id', 'user_plan_id', 'route_id', 'track_id', 'reservation_start', 'reservation_end', 'route_stop_origin_id',
        'route_stop_destination_id', 'created_at', 'updated_at', 'deleted_at'
    ];

}
