<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Routes extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    //protected $collection = 'routes';
    protected $fillable = [
        "id", "external_id", "invitation_code", "title", "start_timestamp", "end_timestamp"
    ];
}
