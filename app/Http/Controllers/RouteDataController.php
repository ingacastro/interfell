<?php

namespace App\Http\Controllers;

use App\Models\RouteData;
use Illuminate\Http\Request;

class RouteDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return RouteData::all();
        $data = RouteData::join('calendar', 'calendar.id', '=', 'route_data.calendar_id')
            ->join('routes', 'route_data.route_id', '=', 'routes.id')
            ->get(['routes.title',
                'route_data.mon', 'route_data.tue', 'route_data.wed',
                'route_data.thu', 'route_data.fri', 'route_data.sat',
                'route_data.sun'
            ]);
        $array = json_decode($data);
        for($i=0; $i<count($data); $i++)
        {
            foreach($data[$i] as $key => $value)
            {
                if($value!=0)
                {
                    echo $data[$i]['title']." ";
                    echo "Incompleta \n";
                    break;
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RouteData  $routeData
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = RouteData::join('calendar', 'calendar.id', '=', 'route_data.calendar_id')
        ->join('routes', 'route_data.route_id', '=', 'routes.id')
        ->where('routes.id', '=', $id)
        ->get(['routes.title',
            'route_data.mon', 'route_data.tue', 'route_data.wed',
            'route_data.thu', 'route_data.fri', 'route_data.sat',
            'route_data.sun'
        ]);
        
        $array = json_decode($data);
        
        for($i=0; $i<count($data); $i++)
        {           
            foreach($data[$i] as $key => $value)
            {
                if($value != 0)
                {
                    echo $data[$i]['title']." ";
                    echo "Incompleta \n";
                    break;
                }
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RouteData  $routeData
     * @return \Illuminate\Http\Response
     */
    public function edit(RouteData $routeData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RouteData  $routeData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RouteData $routeData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RouteData  $routeData
     * @return \Illuminate\Http\Response
     */
    public function destroy(RouteData $routeData)
    {
        //
    }
}
