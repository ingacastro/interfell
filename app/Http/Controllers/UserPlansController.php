<?php

namespace App\Http\Controllers;

use App\Models\UserPlans;
use Illuminate\Http\Request;

class UserPlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserPlans::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserPlans  $userPlans
     * @return \Illuminate\Http\Response
     */
    public function show(UserPlans $userPlans)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserPlans  $userPlans
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPlans $userPlans)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserPlans  $userPlans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPlans $userPlans)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserPlans  $userPlans
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPlans $userPlans)
    {
        //
    }
}
