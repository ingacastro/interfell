<?php

namespace App\Http\Controllers;

use App\Models\Reservations;
use App\Models\UserPlans;
use Illuminate\Http\Request;

class ReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* return Reservations::orderBy('reservation_start', 'asc')
            ->get(['reservation_start']);
        */
       
        $data = Reservations::join('user_plans', 'reservations.user_plan_id', '=', 'user_plans.id')
            ->join('users', 'user_plans.user_id', '=', 'users.id')
            ->get(['users.first_name', 'users.last_name', 'reservations.reservation_start']);
            
        if($data)
        {
            for($i=0; $i<count($data); $i++){
                $timestamp = strtotime($data[$i]['reservation_start']);
                $day = date('l Y-m-d', $timestamp);
                $data[$i]['reservation_start'] = $day;

            }
         
            return $data;
        }
        else
        {
            return json_encode(array('success'=>0,'data'=>[],'message'=>'failure'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservations  $reservations
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data = Reservations::join('user_plans', 'reservations.user_plan_id', '=', 'user_plans.id')
            ->join('users', 'user_plans.user_id', '=', 'users.id')
            ->where('user_plans.user_id', '=',  $id)
            ->get(['users.first_name', 'users.last_name', 'reservations.reservation_start']);
            
        if($data)
        {
            for($i=0; $i<count($data); $i++){
                $timestamp = strtotime($data[$i]['reservation_start']);
                $day = date('l Y-m-d', $timestamp);
                $data[$i]['reservation_start'] = $day;
            }
         
            return $data;
        }
        else
        {
            return json_encode(array('success'=>0,'data'=>[],'message'=>'failure'));
        }
        
        //return Reservations::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reservations  $reservations
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservations $reservations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reservations  $reservations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservations $reservations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservations  $reservations
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservations $reservations)
    {
        //
    }
}
