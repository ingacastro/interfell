<?php

namespace App\Http\Controllers;

use App\Models\CalendarDaysDisabled;
use Illuminate\Http\Request;

class CalendarDaysDisabledController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = CalendarDaysDisabled::join('calendar', 'calendar.id', '=', 'calendar_days_disabled.calendar_id')
        ->join('route_data', 'route_data.calendar_id', '=', 'calendar_days_disabled.calendar_id')
        ->join('routes', 'routes.id', '=', 'route_data.route_id')
        ->get(['name','day', 'title']);
        
        foreach($data as $dato)
        {
            $ts = strtotime($dato['day']);
            echo "Días no disponibles para: ".$dato['title']." ".date("l d-m-Y", $ts)."\n";
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CalendarDaysDisabled  $calendarDaysDisabled
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = CalendarDaysDisabled::join('calendar', 'calendar.id', '=', 'calendar_days_disabled.calendar_id')
        ->join('route_data', 'route_data.calendar_id', '=', 'calendar_days_disabled.calendar_id')
        ->join('routes', 'routes.id', '=', 'route_data.route_id')
        ->where('routes.id', '=', $id)
        ->get(['name','day', 'title']);
        
        foreach($data as $dato)
        {
            $ts = strtotime($dato['day']);
            echo "Días no disponibles para: ".$dato['title']." ".date("l d-m-Y", $ts)."\n";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CalendarDaysDisabled  $calendarDaysDisabled
     * @return \Illuminate\Http\Response
     */
    public function edit(CalendarDaysDisabled $calendarDaysDisabled)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CalendarDaysDisabled  $calendarDaysDisabled
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CalendarDaysDisabled $calendarDaysDisabled)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CalendarDaysDisabled  $calendarDaysDisabled
     * @return \Illuminate\Http\Response
     */
    public function destroy(CalendarDaysDisabled $calendarDaysDisabled)
    {
        //
    }
}
