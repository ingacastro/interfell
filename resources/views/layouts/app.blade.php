<html>
    <head>
        <title>@yield('title')</title>
        <link href="/css/app.css" rel="stylesheet">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>

