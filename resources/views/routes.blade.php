@extends('layouts.app')

@section('title', 'Routes')
@section('content')
    <div class="row">
        <div class="col">id</div>
        <div class="col">external_id</div>
        <div class="col">invitation_code</div>
        <div class="col">title</div>
    </div>
    @foreach($routes as $route)
    <div class="row">
        <div class="col">{{$route->id}}</div>
        <div class="col">{{$route->external_id}}</div>
        <div class="col">{{$route->invitation_code}}</div>
        <div class="col">{{$route->title}}</div>
    </div>
    @endforeach
@stop