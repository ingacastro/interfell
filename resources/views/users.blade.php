@extends('layouts.app')

@section('title', 'Users')

@section('content')
    @foreach($users as $user)
    <div class="row">
        <div class="col">{{$user->id}}</div>
        <div class="col">{{$user->first_name}}</div>
        <div class="col">{{$user->last_name}}</div>
        <div class="col">{{$user->phone_number}}</div>
    </div>
    @endforeach
@stop