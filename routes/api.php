<?php

use App\Http\Controllers\ReservationsController;
use App\Http\Controllers\CalendarDaysDisabledController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\RoutesController;
use App\Http\Controllers\RouteDataController;
use App\Http\Controllers\UserPlansController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('reservations', ReservationsController::class);
Route::resource('users', UsersController::class);
Route::resource('routes', RoutesController::class);
Route::resource('routeData', RouteDataController::class);
Route::resource('calendarDaysDisabled', CalendarDaysDisabledController::class);
Route::resource('userPlans', UserPlansController::class);
